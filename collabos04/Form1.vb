﻿Public Class Form1
    Public players As New List(Of Player)


    Public Function getRandom() As Integer
        Static tmp As New System.Random
        Return tmp.Next(0, 2)
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ListBox1.Items.Clear
        players.Clear()
        players.Add(New Player("Player 1"))
        players.Add(New Player("Player 2"))
        Dim isGameOver = False
        Do Until isGameOver = True
            Dim rez As String = ""
            Dim rnd As Integer = getRandom()
            players(rnd).getpoint()
            players(rnd).deuce = False
            If Not players(0).deuce = players(1).deuce Then
                For Each p As Player In players
                    p.point -= 1
                    p.deuce = False
                Next
            End If
            If players(0).point = players(1).point Then
                If players(0).point < 3 And players(1).point < 3 Then
                    rez = players(0).returnPoints & "-all"
                Else
                    rez = "deuce"
                    players(0).deuce = True
                    players(1).deuce = True
                End If
            Else
                rez = players(0).returnPoints & "-" & players(1).returnPoints
            End If
            If rez.Contains("advantage") Then
                For Each p As Player In players
                    If p.returnPoints.Contains("advantage") Then rez = p.returnPoints
                Next
            End If

            For Each p As Player In players
                If p.point = 4 Then isGameOver = True
            Next
            If isGameOver = True Then
                For Each p As Player In players
                    If p.point = 4 Then rez = p.returnPoints
                Next
            End If
            ListBox1.Items.Add(rez)
        Loop
    End Sub
End Class
