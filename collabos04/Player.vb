﻿Public Class Player
    Public point As Integer
    Public name As String
    Public deuce = False

    Public Sub New(name As String)
        point = 0
        Me.name = name
    End Sub

    Public Sub getpoint()
        point += 1
    End Sub

    Public Function returnPoints() As String
        If point = 0 Then Return "love"
        If point = 1 Then Return "fifteen"
        If point = 2 Then Return "thirty"
        If point = 3 Then Return "advantage " & name
        If point = 4 Then Return "win for " & name
        Return Nothing
    End Function
End Class
